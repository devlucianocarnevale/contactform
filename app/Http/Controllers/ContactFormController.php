<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create(Request $request)
    {
        //Obtiene todas las variables del objeto contacto
        $name = ($request->input("name") != null) ? $request->input("name") : "";
        $lastName = ($request->input("lastName") != null) ? $request->input("lastName") : "";
        $email = ($request->input("email") != null) ? $request->input("email") : "";
        $dni = ($request->input("dni") != null) ? $request->input("dni") : -1;
        $description = ($request->input("description") != null) ? $request->input("description") : "";
        
        //Instancia un nuevo objeto contacto
        $contact = new \App\Models\Contact();
        
        //Asigna propiedades del objeto
        $contact->name = $name;
        $contact->last_name = $lastName;
        $contact->email = $email;
        $contact->dni = $dni;
        $contact->description = $description;
        
        //Guarda objeto
        $contact->save();
        
        //Devuelve el objeto
        return json_encode($contact);
        
    }
    
    public function existDni($dni)
    {   
        //Chequea en el modelo si ya existe un registro con este DNI
        $contact = \App\Models\Contact::where("dni",$dni)->first();
        
        //Devuelve true si el dni ya existe en algun contacto . False de lo contrario
        return ($contact != null);
    }

    public function existDniAjax(Request $request)
    {   
        //Obtiene el numero de DNI del request
        $dni = ($request->input("dni") != null) ? $request->input("dni") :  0;
        
        //Invoca al metodo para verificar si el DNI existe
        $ret = $this->existDni($dni);
        
        //Devulve true o false segun corresponda
        return $ret;
    }
    
    
}
