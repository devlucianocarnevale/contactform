//Control una vez cargada la pagina
$( document ).ready(function() {
    
    //Eventhandler para verificar que el email sea correcto
    $( "#email" ).focusout(function() {
        var email;
        email = $("#email").val();
        //Si el email ingresado no cumple el formato
        if(!isEmail(email))
        {
            msgbox("Debe ingresar un e-mail en formato correcto")
        }
    });
    
    //Eventhandler para verificar que el DNI ingresado no exista.
    $( "#dni" ).focusout(function() {
        var dni;
        dni = $("#dni").val();
        checkDni(dni);
    });
    
     //Eventhandler para envio del formulario al backend
    $( "#btnSend" ).click(function() {
        sendForm();
    });
    
      //Eventhandler para limpiar formulario
    $( "#btnClear" ).click(function() {
        clearForm();
    });
     
     
});

//Muestra/oculta y setea el texto del label del DNI con error
function showDniValidation(text)
{
    if(text != "")
    {
        $("#dniValidate").removeClass("d-none");
        $("#dniValidate").addClass("d-block");
        $("#dniValidate").html(text);
    }
    else
    {
        $("#dniValidate").removeClass("d-block");
        $("#dniValidate").addClass("d-none");
        $("#dniValidate").html("");
    }
}

//Funcion para verificar si el dni ingresado ya existe.
function checkDni(dni)
{
    try 
    {
        //Texto para mostrar si hay error
        var text = "El DNI " + dni + " ya existe. Por favor ingrese uno diferente";
        //Ajax para verifica en la base
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "checkDni",
            method: "POST",
            data: { "dni": dni },
            success: function(result){
                if(result)
                {
                    //Muestra mensaje
                    msgbox(text);
                    //setea label dni error
                    showDniValidation(text);
                    //vacia el campo
                    $("#dni").val("");
                    
                }
                else
                {
                    //Si el dni no existe en la base limpia el campo
                    showDniValidation("");
                }
            }
        });
    } 
    catch (error) {
        console.error(error);
    }
}

//Valida que todos los campos esten completos antes de enviar. Tambien que el email sea valido.
function validateForm()
{
    try 
    {
        //Obtiene los valores del formulario
        var name = $("#name").val();
        var lastName = $("#lastName").val();
        var dni = $("#dni").val();
        var email = $("#email").val();
        var description = $("#description").val();
        
        //No hay reglas rotas sin error
        var brokenRules = "";
        
        if(name == "")
        {
            brokenRules += "Debe indicar su NOMBRE <br/>";
        }
        if(lastName == "")
        {
            brokenRules += "Debe indicar su APELLIDO<br/>";
        }
        if(dni == "")
        {
            brokenRules += "Debe indicar su DNI<br/>";
        }
        if(email == "")
        {
            brokenRules += "Debe indicar su EMAIL<br/>";
        }
        if(description == "")
        {
            brokenRules += "Debe indicar una CONSULTA<br/>";
        }
        
        if(!isEmail(email))
        {
            brokenRules += "La direccion de correo eletronico no es valida<br/>";
        }
        
        
        //devuelva la concatenacion de reglas rotas
        return brokenRules;
    } 
    catch (error) {
        console.error(error);
    }
}

//valida email
function isEmail(email) 
{
    //Verificar regular expression para email
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

//Funcion para mensaje modal
function msgbox(message)
{
    $("#modal-body").html(message);
    $("#msgbox").modal("show");
}

//Funcion para enviar el formulario al backend
function sendForm()
{
    try 
    {
        //Obtiene reglas no cumplidas
        var brokenRules = this.validateForm();
        
        //si hay alguna regla sin cumplir las informa y cancela el envio del formulario
        if(brokenRules != "")
        {
            msgbox(brokenRules);
            return;
        }
        
        //Si cumple las reglas, obtiene todas las variables
        var name = $("#name").val();
        var lastName = $("#lastName").val();
        var dni = $("#dni").val();
        var email = $("#email").val();
        var description = $("#description").val();
        
        
        //Ajax para guardar
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: "saveForm",
            method: "POST",
            data: { "name": name, "lastName": lastName, "email": email, "dni": dni, "description": description },
            success: function(result){
                var obj;
                //Convierte a Objeto
                obj = JSON.parse(result);
                //Si guardo correctamente, va a haber devuelto el id
                if(obj.dni > 0)
                {
                    msgbox("El contacto se ha guardado exitosamente. A la brevedad nos contacteremos a su email: " + obj.email);
                    clearForm();
                }
            }
        });
    } 
    catch (error) {
        console.error(error);
    }
}

//Funcion para limpiar formulario
function clearForm()
{
    try 
    {
        
        //Reset form
        $("#name").val("");
        $("#lastName").val("");
        $("#dni").val("");
        $("#email").val("");
        $("#description").val("");
        
    } 
    catch (error) {
        console.error(error);
    }
}