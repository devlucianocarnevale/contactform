<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Contacto</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
       
        
        <script src="js/manager.js"></script>
        
    </head>
    <body class="">
        <div class="modal" tabindex="-1" role="dialog" id="msgbox">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header bg-info text-white">
              <h5 class="modal-title">Informaci&oacute;n</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="text-white">&times;</span>
              </button>
            </div>
            <div class="modal-body" id="modal-body">
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
          </div>
        </div>
      </div>
        
        <div class="row">
            <div class="col-12 p-5 mx-auto">
                <div class="col-6 mx-auto p-5 border rounded">
                    <div class="container-fluid">
                        <div class="row mb-2">
                                <div class="col-4 small">
                                     Nombre * 
                                </div>
                                <div class="col-8">
                                    <input class="form-control col" id="name" placeholder="Nombre"/>
                                </div>
                        </div>
                       <div class="row mb-2">
                                <div class="col-4 small">
                                    Apellido *
                                </div>
                                <div class="col-8">
                                    <input class="form-control col" id="lastName" placeholder="Apellido"/>
                                </div>
                        </div>
                        <div class="row mb-2">
                                <div class="col-4 small">
                                    E-mail * 
                                </div>
                                <div class="col-8">
                                    <input class="form-control col" id="email" placeholder="Correo electronico de contacto"/>
                                </div>
                        </div>
                        <div class="row mb-2">
                                <div class="col-4 small">
                                    DNI * 
                                </div>
                                <div class="col-8">
                                    <input class="form-control col" id="dni" placeholder="Numero de DNI sin puntos"/>
                                    <div class="d-none text-danger mx-auto small col" id="dniValidate">
                                </div>
                            
                                
                            </div>
                        </div>
                        <div class="row mb-2">
                                <div class="col-4 small">
                                    Consulta * 
                                </div>
                                <div class="col-8">
                                    <textarea class="form-control col-12" id="description" placeholder="Describa su consulta por favor"></textarea>
                                </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col">
                                <div class="small muted">* campos obligatorios</div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-6">
                                <button class="btn btn-info col" id="btnSend">Enviar</button>
                            </div>
                            <div class="col-6">
                                <button class="btn btn-warning col" id="btnClear">Reiniciar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
